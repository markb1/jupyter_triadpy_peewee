#!/bin/bash

NB_UID=${NB_UID:-65534}
NB_GID=${NB_GID:-100}

NB_USER=${NB_USER:-"jovyan"}

echo "Starting with UID : $NB_USER"
export HOME=/home/${NB_USER}
export USER=${NB_USER}

exec setpriv --reuid=${NB_UID} --regid=${NB_GID} --clear-groups "$@"
