
::

  mbiggers@knuth ~/g/P/J/triad_python_peewee% docker run -it --rm -p 10000:8888 -v $PWD/:/home/jovyan/work jupyter/minimal-notebook
  Unable to find image 'jupyter/minimal-notebook:latest' locally
  latest: Pulling from jupyter/minimal-notebook
  ...

  Executing the command: jupyter notebook
  [I 15:37:35.875 NotebookApp] Writing notebook server cookie secret to /home/jovyan/.local/share/jupyter/runtime/notebook_cookie_secret
  [I 15:37:36.212 NotebookApp] JupyterLab extension loaded from /opt/conda/lib/python3.7/site-packages/jupyterlab
  [I 15:37:36.212 NotebookApp] JupyterLab application directory is /opt/conda/share/jupyter/lab
  [I 15:37:36.214 NotebookApp] Serving notebooks from local directory: /home/jovyan
  [I 15:37:36.214 NotebookApp] The Jupyter Notebook is running at:
  [I 15:37:36.214 NotebookApp] http://(54059e8a78ea or 127.0.0.1):8888/?token=de70c693ac1d880f4f23e939c5b79d2c1670c9fb578f5dfc
  [I 15:37:36.214 NotebookApp] Use Control-C to stop this server and shut down all kernels (twice to skip confirmation).
  [C 15:37:36.215 NotebookApp] 

  Copy/paste this URL into your browser when you connect for the first time,
  to login with a token:
  http://(54059e8a78ea or 127.0.0.1):8888/?token=de70c693ac1d880f4f23e939c5b79d2c1670c9fb578f5dfc
  http://127.0.0.1:8888/?token=de70c693ac1d880f4f23e939c5b79d2c1670c9fb578f5dfc


