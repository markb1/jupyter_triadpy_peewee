# triad_python_peewee/Dockerfile  -*-conf-*-
# -- image-ref: https://hub.docker.com/_/python/
# -- partly based on:
#    https://github.com/jupyter/docker-stacks/tree/master/base-notebook

FROM python:3.7-slim-stretch

ENV PYTHONUNBUFFERED 1
ENV USER root

LABEL maintainer="Mark Biggers <biggers@utsl.com>"

ARG NB_USER="jovyan"
# nobody.users
ARG NB_UID="65534"
ARG NB_GID="100"
ARG NB_UMASK="0000"

# temporary ...
USER root

# fix(es) for /etc/default/locale
RUN apt-get -y update && \
    apt-get -y install locales locales-all && \
    update-locale LANG=en_US.UTF-8

# Configure environment
ENV SHELL=/bin/bash \
    NB_USER=$NB_USER \
    NB_UID=$NB_UID \
    NB_GID=$NB_GID \
    NB_UMASK=$NB_UMASK \
    LANG=en_US.UTF-8 \
    LANGUAGE=en_US.UTF-8 \
    LC_ALL=en_US.UTF-8 \
    HOME=/home/$NB_USER

RUN apt-get -y install --no-install-recommends \
    bzip2 ca-certificates fonts-liberation setpriv wget \
    netcat less vim-tiny jove make procps psmisc && \
    /bin/rm -rf /var/lib/apt/lists/*

COPY requirements.txt /tmp
RUN pip3 install --no-cache-dir -r /tmp/requirements.txt

ADD fix-permissions /usr/local/bin/fix-permissions

# Create 'jovyan' user with UID=$NB_UID and in the 'users' group
# and make sure these dirs are writable by the 'users' group
RUN useradd -o -m -s /bin/bash -N -u $NB_UID -g $NB_GID $NB_USER && \
    chmod g+w /etc/passwd

COPY entrypoint.sh /usr/local/bin/entrypoint.sh

USER $NB_UID:$NB_GID

# Setup work directory for backward-compatibility
RUN mkdir -p $HOME/work

COPY . $HOME/work
WORKDIR $HOME

# RUN fix-permissions $HOME

USER root

# Switch back to jovyan to avoid accidental container runs as root
# ENV USER=$NB_USER
# USER $NB_UID:$NB_GID

EXPOSE 8888

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
# CMD["jupyter notebook --ip=0.0.0.0 --port=8888"]

